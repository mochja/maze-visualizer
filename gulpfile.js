var gulp = require('gulp');
var concat = require('gulp-concat');
var useref = require('gulp-useref');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var revReplace = require('gulp-rev-replace');
var rev = require('gulp-rev');
var clean = require('gulp-clean');

gulp.task('clean-dist', function () {
  return gulp.src('dist/*')
    .pipe(clean({force: true}));
});

gulp.task('build', function () {
  var assets = useref.assets();

  return gulp.src('app/*.html')
    .pipe(assets)
    .pipe(gulpif('*.js', uglify()))
    .pipe(rev())
    .pipe(assets.restore())
    .pipe(useref())
    .pipe(revReplace())
    .pipe(gulp.dest('dist'));
});

gulp.task('default', ['clean-dist', 'build']);