var stage = new PIXI.Stage(0xFFFFFF);
var renderer = PIXI.autoDetectRenderer(320, 320, { antialias: true, resolution: 1 });

document.querySelector('.render-view').appendChild(renderer.view);

requestAnimFrame( animate );

function showMaze() {
  ga('send', 'event', 'maze', 'render');

  var mazeData = document.getElementById('maze').value.split('\n');
  var pathData = document.getElementById('path').value.split('\n');
  var mazeDimensions = mazeData[0].split(' ');
  var maze = {
    rows: parseInt(mazeDimensions[0], 10),
    cols: parseInt(mazeDimensions[1], 10),
    cells: _.map(mazeData.slice(1), function (data) {
      return _.map( data.split(' '), function( val ) { return parseInt( val, 10 ); } );
    })
  };

  var sum = _.reduce( _.map(maze.cells, function (data) {
    return data.length;
  }), function(memo, num) { return memo + num; }, 0);

  if ( maze.rows !== mazeData.length-1 || sum/maze.rows !== maze.cols ) {
    alert('Check your map file.');
    return;
  }

  stage.removeChildren();
  renderer.resize( ( maze.cols % 2 ? maze.cols : maze.cols + 1)*50+90+( maze.cols % 2 ? 0 : -50 ) , maze.rows*MazePrinter.TRIANGLE_HEIGHT+70);

  pathData = _.map( pathData, function (pos) {
    return _.map(pos.split(','), function ( val ) { return parseInt(val, 10) - 1; });
  } );

  var triangels = [];
  var path = [];
  var titles = new PIXI.Graphics();
  for (var i = 0; i < maze.rows; i++) {
    for (var j = 0; j < maze.cols; j++) {
      var flip = (i % 2) == 0;
      flip = j%2 ? !flip : flip;
      var triangle = MazePrinter.createTriangle(20+j*50, 100+i*MazePrinter.TRIANGLE_HEIGHT, maze.cells[i][j], flip);
      titles.addChild( MazePrinter.drawTriangleTitle((i+1)+','+(j+1), 20+j*50, 100+i*MazePrinter.TRIANGLE_HEIGHT, flip ? -10 : 10) );
      triangels.push(triangle);
    }
  }

  var lines = _.map( triangels, function ( triangle ) {
    return triangle.path;
  } );

  var borders = _.map( triangels, function ( triangle ) {
    return triangle.borders;
  } );

  lines = _.uniq( _.flatten(lines, true), function(a) { return a[0].join(',') + ';' + a[1].join(','); } );
  borders = _.uniq( _.flatten(borders, true), function(a) { return a[0].join(',') + ';' + a[1].join(','); } );

  var map = new PIXI.Graphics();

  _.each( lines, function (coord, i) {
    var line = new PIXI.Graphics();
    line.lineStyle(2, 0xE6E6E6);
    line.moveTo(coord[0][0], coord[0][1]);
    line.lineTo(coord[1][0], coord[1][1]);
    map.addChild(line);
  } );

  _.each( borders, function (coord, i) {
    var line = new PIXI.Graphics();
    line.lineStyle(2, 0x000000);
    line.moveTo(coord[0][0], coord[0][1]);
    line.lineTo(coord[1][0], coord[1][1]);
    map.addChild(line);
  } );

  stage.addChild(map);

  path = _.map( pathData, function (coord) {
    var i = coord[0], j = coord[1];
    return [20+j*50+50, 100+i*(MazePrinter.TRIANGLE_HEIGHT)-MazePrinter.TRIANGLE_HEIGHT/2 ];
  });

  if ( path.length > 0 ) {
    var pathRender = new PIXI.Graphics();
    pathRender.moveTo(path[path.length-1][0], path[path.length-1][1]);
    for (var i = path.length - 2; i >= 0; i--) {
      pathRender.lineStyle(2, 0xFF0000);
      pathRender.lineTo(path[i][0], path[i][1]);
      var circle = new PIXI.Graphics();
      circle.lineStyle(2, 0xFF0000);
      circle.beginFill(i == 0 ? 0xFF0000 : 0xFFFFFF);
      circle.drawCircle(path[i][0], path[i][1], 3);
      pathRender.addChild(circle);
    };
    stage.addChild(pathRender);
  }

  stage.addChild(titles);

}

function animate() {
    requestAnimFrame( animate );
    renderer.render(stage);
}

var togglePanel = function () {
  var $el = document.querySelector('.hide-me');
  var style = $el.style.display;
  $el.style.display = (style !== 'none') ? 'none' : 'block';
};