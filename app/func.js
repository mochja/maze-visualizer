PIXI.dontSayHello = true;

MazePrinter = {};

(function (PIXI, lib, _) {

  lib.TRIANGLE_HEIGHT = 65;

  var parseBorder = function (border) {
    return {
      bottom: Boolean(border&0x04),
      top: Boolean(border&0x04),
      left: Boolean(border&0x01),
      right: Boolean(border&0x02)
    };
  };

  lib.createTriangle = function (x, y, border, flip) {
    border = parseBorder(border);
    var path = lib.createTrianglePath(x, y, flip);
    var borders = [];
    var emptyLines = [];
    if ( border.top ) {
      borders.push(path[0]);
    } else {
      emptyLines.push(path[0]);
    }
    if ( border.left ) {
      borders.push(path[2]);
    } else {
      emptyLines.push(path[2]);
    }
    if ( border.right ) {
      borders.push(path[1]);
    } else {
      emptyLines.push(path[1]);
    }
    return {
      borders: borders,
      path: emptyLines
    };
  };

  lib.createTrianglePath = function (x, y, flip) {
    if ( flip ) {
      y -= lib.TRIANGLE_HEIGHT;
    }
    return [ 
    [ [x, y], [x+100, y] ], 
    [ [x+100, y], [x+50, y+(flip ? lib.TRIANGLE_HEIGHT : -lib.TRIANGLE_HEIGHT)] ], 
    [ [x+50, y+(flip ? lib.TRIANGLE_HEIGHT : -lib.TRIANGLE_HEIGHT)], [x, y] ]];
  };

  lib.drawTriangle = function (x, y, border, flip) {
    var graphics = new PIXI.Graphics();
    var border = parseBorder(0);

    graphics.lineStyle(2, 0xEAEAEA);

    if ( flip ) {
      y -= lib.TRIANGLE_HEIGHT;
    }

    graphics.moveTo(x, y);

    if ( border.top ) {
      graphics.lineTo(x+100, y);
    } else {
      graphics.lineTo(x+100, y);
    }

    if ( flip ) {
      if ( border.right ) {
        graphics.moveTo(x+50, y+lib.TRIANGLE_HEIGHT);
      } else {
        graphics.lineTo(x+50, y+lib.TRIANGLE_HEIGHT);
      }
    } else {
      if ( border.right ) {
        graphics.moveTo(x+50, y-lib.TRIANGLE_HEIGHT);
      } else {
        graphics.lineTo(x+50, y-lib.TRIANGLE_HEIGHT);
      }
    }

    if ( border.left ) {
      graphics.addChild( new PIXI.Point(x, y) );
    } else {
      graphics.lineTo(x, y);
    }

    return graphics;
  };

  lib.drawTriangleTitle = function ( text, x, y, offset, inside ) {
    var text = new PIXI.Text(text, {font:'18px Tahoma', fill:'black', align: 'center'/*, strokeThickness: 2, stroke: 'green'*/});
    var textBounds = text.getBounds();
    text.position.x = x+100/2-textBounds.width/2;
    text.position.y = y-lib.TRIANGLE_HEIGHT/2+offset-textBounds.height/2;
    return text;
  }

})(PIXI, MazePrinter, _);